﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace Matrix_Multiplication_Example
{
    class GameObject
    {
        private Model model;
        private Matrix[] transforms;

        public Vector3 position;
        public Vector3 scale = Vector3.One;
        public Vector3 rotation;

        // Behaviour

        public void LoadModel(ContentManager content, string name)
        {
            model = content.Load<Model>(name);

            transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
        }

        public void Draw()
        {
            // Loop through the meshes in the 3D model, drawing each one in turn
            foreach (ModelMesh mesh in model.Meshes)
            {
                // Loop through each effect on the mesh
                foreach (BasicEffect effect in mesh.Effects)
                {
                    // To render we need three things - world matrix, view matrix, and projection matrix
                    // But we actually start in model space - this is where our world starts before transforms

                    // ------------------------------
                    // MESH BASE MATRIX
                    // ------------------------------
                    // Our meshes start with world = model space, so we use our transforms array
                    effect.World = transforms[mesh.ParentBone.Index];


                    // ------------------------------
                    // WORLD MATRIX
                    // ------------------------------
                    // Transform from model space to world space in order - scale, rotation, translation.

                    // Scale
                    // Scale our model by multiplying the world matrix by a scale matrix
                    // XNA does this for use using CreateScale()
                    effect.World *= Matrix.CreateScale(scale);

                    // Rotation
                    // Rotate our model in the game world
                    effect.World *= Matrix.CreateRotationX(rotation.X); // Rotate around the x axis
                    effect.World *= Matrix.CreateRotationY(rotation.Y); // Rotate around the y axis
                    effect.World *= Matrix.CreateRotationZ(rotation.Z); // Rotate around the z axis

                    // Translation / position
                    // Move our model to the correct place in the game world
                    effect.World *= Matrix.CreateTranslation(position);

                    // ------------------------------
                    // VIEW MATRIX
                    // ------------------------------
                    // This puts the model in relation to where our camera is, and the direction of our camera.
                    effect.View = Matrix.CreateLookAt(
                        new Vector3(0, 500, -500),
                        Vector3.Zero, // target
                        Vector3.Up
                        );

                    // ------------------------------
                    // PROJECTION MATRIX
                    // ------------------------------
                    // Projection changes from view space (3D) to screen space (2D)
                    // Can be either orthographic or perspective

                    // Perspective
                    //effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                    //    MathHelper.ToRadians(45),
                    //    16f/9f,
                    //    1f,
                    //    100000f);

                    // Orthographic
                    effect.Projection = Matrix.CreateOrthographic(
                        1600, 900, 1f, 10000f
                        );

                    // ------------------------------
                    // LIGHTING
                    // ------------------------------
                    // Some basic lighting, feel free to tweak and experiment
                    effect.LightingEnabled = true;
                    effect.Alpha = 1.0f;
                    effect.AmbientLightColor = new Vector3(1.0f);
                }

                // Draw the current mesh using the effects we set up
                mesh.Draw();
            }
        }

    }
}
